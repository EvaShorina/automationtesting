package org.example;




import org.json.JSONArray;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import io.restassured.response.Response;
import io.restassured.RestAssured;
import io.restassured.specification.RequestSpecification;
import org.json.JSONObject;




public class AppTest
{

    @Test
    public void shouldAnswerWithTrue()
    {
        JSONObject requestBody = new JSONObject();

        requestBody.put("folderId", "b1g8kgqu7hpabuej829r");
        requestBody.put("texts","Hello World");
        requestBody.put("targetLanguageCode", "ru");

        RequestSpecification request = RestAssured.given();
        request.header("content-type","application/json").header("Authorization","Bearer t1.9euelZqUzouUnMbGnZTKx4_Li4zIze3rnpWaxpzIxpTHyZOKysuNk8uKks_l8_d1Bi15-e8cBTRY_N3z9zU1Knn57xwFNFj8.dKw-sYn5KPdxK4v6ie61tKmCL5syBBxyvMVD-YkixbAbCCaSByvvB52dZzjq6R_wvuClYMnds3jAXG9FItXhBA");
        request.body(requestBody.toString());



        Response response = request.post("https://translate.api.cloud.yandex.net/translate/v2/translate");
        String json = response.asString();
        JSONObject obj = new JSONObject(json);
        JSONArray arr = obj.getJSONArray("translations");
        String text = arr.getJSONObject(0).getString("text");


        assertEquals(text, "Привет, Мир");
    }
}
